import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialUiComponent } from './materialui.component';
import { MaterialModule } from 'src/app/core/material.module';



@NgModule({
  declarations: [
    MaterialUiComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ]
})
export class MaterialUiModule { }
