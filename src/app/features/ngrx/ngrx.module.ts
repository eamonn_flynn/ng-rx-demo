import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgrxRoutingModule } from './ngrx-routing.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NgrxRoutingModule
  ]
})
export class NgrxModule { }
