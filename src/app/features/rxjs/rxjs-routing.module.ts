import {  NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BehaviourSubjectComponent } from './behavioursubject/behavioursubject.component';
import { DemoOneComponent } from './demoone/demoone.component';
import { RxjsComponent } from './rxjs.component';
import { SubjectComponent } from './subject/subject.component';


const subRoutes: Routes = [
  {
    path: '',
    component: RxjsComponent,
    children: [
      { path: 'demoone', component: DemoOneComponent },
      { path: 'subject', component: SubjectComponent },
      {
        path: 'behavioursubject',
        component: BehaviourSubjectComponent,

      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(subRoutes)
  ],
  exports: [RouterModule]
})
export class RxjsRoutingModule {}
