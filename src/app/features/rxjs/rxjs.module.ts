import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RxjsRoutingModule } from './rxjs-routing.module';
import { RxjsComponent } from './rxjs.component';
import { SubjectComponent } from './subject/subject.component';
import { DemoOneComponent } from './demoone/demoone.component';
import { BehaviourSubjectComponent } from './behavioursubject/behavioursubject.component';
import { MaterialModule } from 'src/app/core/material.module';



@NgModule({
  declarations: [
    RxjsComponent,
    SubjectComponent,
    DemoOneComponent,
    BehaviourSubjectComponent

  ],
  imports: [
    CommonModule,
    RxjsRoutingModule,
    MaterialModule


  ],
  exports:[
    RxjsRoutingModule
  ]

})
export class RxjsModule { }
