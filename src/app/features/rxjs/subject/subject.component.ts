import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.scss']
})
export class SubjectComponent implements OnInit {



  constructor() { }

  ngOnInit(): void {

    const subject$ = new Subject();
    subject$.subscribe(x=> console.log('first subscribe',x))
    subject$.next(1);
    subject$.next(2);

    // use unsubscribe over complete to get an error and spot when  subjects are being called but are no longer available
    subject$.subscribe(x=> console.log('2nd subscribe',x))
    subject$.next(3);

  }

  ngOnDestroy(){

  }


}
