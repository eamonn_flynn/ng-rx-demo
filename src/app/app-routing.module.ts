import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [

  {
    path: '', pathMatch: 'full', redirectTo: 'home'
  },
  {
    path: 'home', loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule)
  },
  { path: 'materialui', loadChildren: () => import('./features/materialui/materialui.module').then(m => m.MaterialUiModule) },
  { path: 'ngrx', loadChildren: () => import('./features/ngrx/ngrx.module').then(m => m.NgrxModule) },
  { path: 'rxjs', loadChildren: () => import('./features/rxjs/rxjs.module').then(m => m.RxjsModule) },

];

@NgModule({

  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
